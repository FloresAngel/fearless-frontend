import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        {/* <LocationForm /> */}
        {/* <ConferenceForm /> */}
        <PresentationForm/>
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}

export default App;
// import LocationForm from './LocationForm';
// import ConferenceForm from './ConferenceForm';
// import AttendConferenceForm from './AttendConferenceForm';
// import PresentationForm from './PresentationForm';
// import { BrowserRouter, Route, Routes } from "react-router-dom";

// function App(props) {
//     <Nav/>
//     <div className="container">
//       <Routes>
//         <Route path="presentations">
//           <Route path="new" element={<PresentationForm />} />
//         </Route>
//         <Route path="locations">
//           <Route path="new" element={<LocationForm />} />
//         </Route>
