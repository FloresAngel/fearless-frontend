import React, { useEffect, useState } from 'react';

function PresentationForm() {
    // State variables
    const [presenterName, setName] = useState("");
    const [presenterEmail, setEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("")
    const [synopsis, setSynopsis] = useState("");
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");

    // Fetch conference data from API
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    // Fetch data on component mount
    useEffect(() => {
        fetchData();
    }, []);

    // Form submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Construct data object
        const data = {
            presenter_name: presenterName,
            presenter_email: presenterEmail,
            company_name: companyName,
            title: title,
            synopsis: synopsis,
            conference: conference
        };

        // API endpoint for creating presentation
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;

        // Fetch configuration
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // Send POST request to create presentation
        const response = await fetch(presentationUrl, fetchConfig);
        console.log(response);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            // Reset form fields
            setName("");
            setEmail("");
            setCompanyName("");
            setTitle("");
            setSynopsis("");
            setConference("");
        }
    }

    // Event handlers for form input changes
    const handleNameChange = (event) => setName(event.target.value);
    const handleEmailChange = (event) => setEmail(event.target.value);
    const handleCompanyChange = (event) => setCompanyName(event.target.value);
    const handleTitleChange = (event) => setTitle(event.target.value);
    const handleSynopsisChange = (event) => setSynopsis(event.target.value);
    const handleConferenceChange = (event) => setConference(event.target.value);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>

                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        {/* Presenter Name */}
                        <div className="form-floating mb-3">
                            <input value={presenterName} onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Name</label>
                        </div>

                        {/* Presenter Email */}
                        <div className="form-floating mb-3">
                            <input value={presenterEmail} onChange={handleEmailChange} placeholder="Presenter email" required type="text" name="presenter_email" id="presenter_email" className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>

                        {/* Company Name */}
                        <div className="form-floating mb-3">
                            <input value={companyName} onChange={handleCompanyChange} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control" />
                            <label htmlFor="company_name">Company Name</label>
                        </div>

                        {/* Title */}
                        <div className="form-floating mb-3">
                            <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="ctitle" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>

                        {/* Synopsis */}
                        <div className="mb-3">
                            <textarea value={synopsis} onChange={handleSynopsisChange} placeholder="Synopsis" required type="text" name="synopsis" id="synopsis" className="form-control"></textarea>
                        </div>

                        {/* Conference Selection */}
                        <div className="mb-3">
                            <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => (
                                    <option key={conference.id} value={conference.id}>
                                        {conference.name}
                                    </option>
                                ))}
                            </select>
                        </div>

                        {/* Submit Button */}
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;