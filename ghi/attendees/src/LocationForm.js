import React, {useEffect, useState} from 'react';

function LocationForm() {
    const [states, setStates] = useState([]); // New state for storing states data
    const [name, setName] = useState(''); // State for storing name input value
    const [roomCount, setRoomCount] = useState(''); // State for storing room count input value
    const [city, setCity] = useState(''); // State for storing city input value
    const [selectedState, setSelectedState] = useState(''); // State for storing selected state
    // Fetch states data from API
    const fetchData = async () => {
        try {
            const url = 'http://localhost:8000/api/states/';
            const response = await fetch(url);
            //if the connection is success
            if (response.ok) {
                const data = await response.json();
                setStates(data.states);
            } else {
                throw new Error('Failed to fetch states');
            }
        } catch (error) {
            console.error('Error fetching states:', error);
        }
    };
    useEffect(() => {
        fetchData();// Fetch states data when component mounts
    }, []);
    // Created handleNameChange method & store it in the state's "name" variable   
    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    // Created handleRoomCountChange method & store it in the state's "roomcount" variable 
    const handleRoomCountChange = (event) => {
        setRoomCount(event.target.value);
    };
    // Created handleCityChange method & store it in the state's "city" variable 
    const handleCityChange = (event) => {
        setCity(event.target.value);
    };
    // Created handleStateChange method & store it in the state's "selectedState" variable 
    const handleStateChange = (event) => {
        setSelectedState(event.target.value);
    };
    // Handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault();  
    // Create data object with form values    
    const data = { room_count: roomCount, name: name, city: city, state: selectedState };
    console.log(data);

    //POST code from new-location.js
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setStates([]);
      setName('');
      setRoomCount('');
      setCity('');
      setSelectedState('');      
    }
};  
 // Return JSX ...   
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                                type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleRoomCountChange} placeholder="Room count" required 
                                type="number" name="room_count" id="room_count" className="form-control" />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCityChange} placeholder="City" required
                                type="text" name="city" id="city" className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">            
                            <select onChange={handleStateChange} required name="state" id="state" className="form-select">
                                <option value="">Choose a state</option>
                                {states.map(state => (                
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.state}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;