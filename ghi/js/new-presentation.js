window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
  
    const response = await fetch(url);
    //fill the option select with conference's names
    if (response.ok) {
      const data = await response.json();  
      const selectTag = document.getElementById('conference');
      
      for (let conference of data.conferences) {
        // Create an 'option' element
        const option = document.createElement('option');
        // Set the '.value' property of the option element to the conference's Id
        option.value = conference.id;
        // Set the '.innerHTML' property of the option element to the conference's name
        option.innerHTML = conference.name;
         // Append the option element as a child of the select tag
        selectTag.appendChild(option);
      }
    }
      // when the form is submit it trigers this code below 
    const formTag = document.getElementById('create-presentation-form');
    // Get the selected conference id
    const selectTag = document.getElementById('conference');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();// Prevent form submission 
      const formData = new FormData(formTag);// Get form data    
      const json = JSON.stringify(Object.fromEntries(formData)); // Convert form data to JSON 
      //assing the value id to the conforenceId
      const conferenceId = selectTag.options[selectTag.selectedIndex].value;
      // Log the JSON data  
      const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });