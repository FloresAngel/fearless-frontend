function createCard(name, location, description, pictureUrl, startDate, startEnds) {
  //formatting date
  let startDateMDY = `${startDate.getMonth() + 1}/${startDate.getDate()}/${startDate.getFullYear()}`;
  let endsDateMDY = `${startEnds.getMonth() + 1}/${startEnds.getDate()}/${startEnds.getFullYear()}`;
  return `  
    <div class="row row-cols-1 row-cols-md-3 g-4">
      <div class="col">
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">
              <small class="text-muted">${startDateMDY} -- ${endsDateMDY}</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);				
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);         
          const startEnds = new Date(details.conference.ends); 
          const location = details.conference.location.name;         
          const html = createCard(title,location, description, pictureUrl, startDate, startEnds);
          const column = document.querySelector('.col');
					column.innerHTML += html;    
        }
      }
    }
  } catch (e) {    
    const errorAlert = document.getElementById('errorAlert');// Display error message in alert
    errorAlert.innerHTML = `<strong>Error:</strong> ${e.name}, ${e.message}`;
    errorAlert.style.display = 'block';
  }
});
