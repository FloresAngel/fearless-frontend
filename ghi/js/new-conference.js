window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    //fill the option select with locations names
    if (response.ok) {
      const data = await response.json();      
      const selectTag = document.getElementById('location');        
      for (let location of data.locations) {
        // Create an 'option' element
        const optionTag = document.createElement('option');
        // Set the '.value' property of the option element to the location's id
        optionTag.value = location.id;
        // Set the '.innerHTML' property of the option element to the location's name
        optionTag.innerHTML = location.name;
        // Append the option element as a child of the select tag
        selectTag.appendChild(optionTag);
      }
    }
        //when the form is submit it trigers this code below 
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault(); // Prevent form submission       
            const formData = new FormData(formTag);// Get form data         
            const json = JSON.stringify(Object.fromEntries(formData)); // Convert form data to JSON        
            // Log the JSON data
            console.log(json);
            const conferencesUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
              method: "POST",
              body: json,
              headers: {
                'Content-Type': 'application/json',
              },
            };
            const response = await fetch(conferencesUrl, fetchConfig);
            if (response.ok) {
              formTag.reset();
              formData.
              const newConferences = await response.json();
              console.log(newConferences);
            }
        });
      });