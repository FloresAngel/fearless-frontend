window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';  
    const response = await fetch(url);
    //fill the option select with states names
    if (response.ok) {
      const data = await response.json();  
      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        // Create an 'option' element
        const optionTag = document.createElement('option');
        // Set the '.value' property of the option element to the state's abbreviation
        optionTag.value = state.abbreviation;
        // Set the '.innerHTML' property of the option element to the state's name
        optionTag.innerHTML = state.state;
        // Append the option element as a child of the select tag
        selectTag.appendChild(optionTag);
      }
    }
    //when the form is submit it trigers this code below 
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault(); // Prevent form submission       
        const formData = new FormData(formTag);// Get form data         
        const json = JSON.stringify(Object.fromEntries(formData)); // Convert form data to JSON        
        // Log the JSON data
        console.log(json);
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "POST",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
          console.log(newLocation);
        }
    });
  });
  